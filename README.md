# Cities
---

Cities is a simple iOS application, available for iPhone and iPad, showing a list of cities from a JSON file. By clicking on a city item, the application shows a map centered at the specific city.

## Getting Started
---

Download the project from https://bitbucket.org/anchevska/listcitiesfromjson and try the app. Any additional installations are not required.

### Requirements
---
- iOS 10.0+
- Swift 3.0+
- XCode 8.0+

## Unit testing
---
Cities includes unit tests within the Tests subdirectory. These tests can be run simply by executing the test action.

## Authors
---
* **Viktorija Anchevska** - *Initial project*

## Acknowledgments
---
* This project was made because of an assignment for a iOS mobile developer position

