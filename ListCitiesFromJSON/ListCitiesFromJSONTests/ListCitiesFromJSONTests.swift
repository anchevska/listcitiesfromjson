//
//  ListCitiesFromJSONTests.swift
//  ListCitiesFromJSONTests
//
//  Created by Viktorija Ancevska on 8/11/18.
//  Copyright © 2018 Viktorija Ancevska. All rights reserved.
//

import XCTest
@testable import ListCitiesFromJSON

class ListCitiesFromJSONTests: XCTestCase {
    
    var citiesViewController: CitiesViewController!
    
    override func setUp() {
        super.setUp()
        if let controller = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: Constants.cityListControllerIdentifier) as? CitiesViewController {
            citiesViewController = controller
        }
    }
    
    override func tearDown() {
        citiesViewController = nil
        super.tearDown()
    }
    
    func testLoadingJSONFile() {
        CitiesManager.shared.loadCities(fromJSONFile: "wrongJSONTest", completionHandler: {(success) in
            XCTAssertFalse(success, "Reading from wrongJSONTest.json should be unsuccessful since this is not a valid json file")
        })

        CitiesManager.shared.loadCities(fromJSONFile: "blabla", completionHandler: {(success) in
            XCTAssertFalse(success, "Reading from blabla.json should be unsuccessful since this file does not exist")
        })
        
        CitiesManager.shared.loadCities(fromJSONFile: "missingKeysTest", completionHandler: {(success) in
            XCTAssertTrue(success, "Reading from missingKeysTest.json should be successful")
        })
        
        CitiesManager.shared.loadCities(fromJSONFile: "test", completionHandler: {(success) in
            XCTAssertTrue(success, "Reading from test.json should be successful")
        })
    }
    
    func testSorting() {
        CitiesManager.shared.loadCities(fromJSONFile: "test", completionHandler: {(success) in
            let cities = CitiesManager.shared.cities
            let sortedCityNames = ["Aaley", "Aasbuttel", "Aasbüttel", "Aasiaat",  "Aba", "Aba", "Aba", "Bukoba", "Copperbelt Province", "Figuig (Centre)", "Kagera Region", "Livingstone", "Mufulira", "Opiyai", "Ouidah", "Oulad Frej"]
            let sortedCountryCodes = ["LB", "DE", "DE", "GL",  "CN", "HU", "NG", "TZ", "ZM", "MA", "TZ", "ZM", "ZM", "UG", "BJ", "MA"]
            for (index, cityName) in sortedCityNames.enumerated() {
                XCTAssertEqual(cityName, cities[index].name, "\(cities[index].name) should not be at index \(index)")
            }
            for (index, countryCode) in sortedCountryCodes.enumerated() {
                XCTAssertEqual(countryCode, cities[index].countryCode, "\(cities[index].countryCode) should not be at index \(index)")
            }
        })
        
    }
    
    func testGetCity() {
        CitiesManager.shared.loadCities(fromJSONFile: "test", completionHandler: {(success) in
            XCTAssertNil(CitiesManager.shared.getCity(forRow: -1), "Should return nil since -1 is invalid index")
            XCTAssertNil(CitiesManager.shared.getCity(forRow: 20), "Should return nil since 20 is invalid index")
            XCTAssertNotNil(CitiesManager.shared.getCity(forRow: 1), "Should return object since the list has more than 1 object")
        })
    }
    
    func testFilter() {
        CitiesManager.shared.loadCities(fromJSONFile: "test", completionHandler: {(success) in
            CitiesManager.shared.filterCities(withPrefix: "") //empty
            XCTAssertEqual(CitiesManager.shared.searchResults.count, 16, "Filtering result list should have 16 entries")
            CitiesManager.shared.filterCities(withPrefix: "a") //lowercase
            XCTAssertEqual(CitiesManager.shared.searchResults.count, 7, "Filtering result list should have 7 entries")
            CitiesManager.shared.filterCities(withPrefix: "A") //uppercase
            XCTAssertEqual(CitiesManager.shared.searchResults.count, 7, "Filtering result list should have 7 entries")
            CitiesManager.shared.filterCities(withPrefix: "Aa")
            XCTAssertEqual(CitiesManager.shared.searchResults.count, 4, "Filtering result list should have 4 entries")
            CitiesManager.shared.filterCities(withPrefix: "Aas")
            XCTAssertEqual(CitiesManager.shared.searchResults.count, 3, "Filtering result list should have 3 entries")
            CitiesManager.shared.filterCities(withPrefix: "Aasb")
            XCTAssertEqual(CitiesManager.shared.searchResults.count, 2, "Filtering result list should have 2 entries")
            CitiesManager.shared.filterCities(withPrefix: "Aas")
            XCTAssertEqual(CitiesManager.shared.searchResults.count, 3, "Filtering result list should have 3 entries")
            CitiesManager.shared.filterCities(withPrefix: "Aa")
            XCTAssertEqual(CitiesManager.shared.searchResults.count, 4, "Filtering result list should have 4 entries")
            CitiesManager.shared.filterCities(withPrefix: "A")
            XCTAssertEqual(CitiesManager.shared.searchResults.count, 7, "Filtering result list should have 7 entries")
            CitiesManager.shared.filterCities(withPrefix: "o")
            XCTAssertEqual(CitiesManager.shared.searchResults.count, 3, "Filtering result list should have 3 entries")
            CitiesManager.shared.filterCities(withPrefix: "tz") //country code
            XCTAssertEqual(CitiesManager.shared.searchResults.count, 0, "Filtering result list should be empty")
            CitiesManager.shared.filterCities(withPrefix: "*") //special character
            XCTAssertEqual(CitiesManager.shared.searchResults.count, 0, "Filtering result list should be empty")
            CitiesManager.shared.filterCities(withPrefix: "ko") //contains but not prefix
            XCTAssertEqual(CitiesManager.shared.searchResults.count, 0, "Filtering result list should be empty")
        })
    }
    
}
