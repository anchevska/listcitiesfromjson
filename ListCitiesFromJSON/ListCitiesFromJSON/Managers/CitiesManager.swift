//
//  CitiesManager.swift
//  ListCitiesFromJSON
//
//  Created by Viktorija Ancevska on 8/11/18.
//  Copyright © 2018 Viktorija Ancevska. All rights reserved.
//

import UIKit

///Singleton class that contains all methods for manipulating with the list of cities.
class CitiesManager {
    
    static let shared = CitiesManager()
    var cities: [City] = [City]()
    var searchResults: [City] = [City]()
    
    private init() { }
    
    /**
     Populate the cities models from a cities.json file
     
     - Parameters:
        - completionHandler: Callback that returns a bool value indicating if the reading from the JSON file was successful or not
     */
    func loadCitiesFromJSON(completionHandler: @escaping (Bool) -> ()) {
        loadCities(fromJSONFile: Constants.jsonFilename, completionHandler: {(success) in
            completionHandler(success)
        })
    }
    
    /**
     Populate the cities models from a custom JSON file
     
     - Parameters:
        - filename: The name of the JSON file, without any extensions
        - completionHandler: Callback that returns a bool value indicating if the reading from the JSON file was successful or not
     */
    func loadCities(fromJSONFile filename: String, completionHandler: @escaping (Bool) -> ()) {
        cities = [City]()
        searchResults = [City]()
        guard let path = Bundle.main.path(forResource: filename, ofType: Constants.jsonExtension) else {
            completionHandler(false)
            return
        }
        do {
            let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
            let json = try JSONSerialization.jsonObject(with: data, options: .mutableLeaves)
            if let jsonArray = json as? [[String : Any]] {
                for entry in jsonArray {
                    cities.append(City(withDictionary: entry))
                }
                sort()
                searchResults = cities
                completionHandler(true)
                return
            }
        } catch {
            print("error occured \(error.localizedDescription)")
        }
        completionHandler(false)
    }
    
    ///Sort the cities in alphabetical order, city first and country after
    fileprivate func sort() {
        cities = cities.sorted(by: {
            return $0.name == $1.name ? $0.countryCode < $1.countryCode : $0.name < $1.name
        })
    }
    
    
    /**
     Filter the cities list by a given prefix. The filtered array is searchResults
     
     - Parameters:
        - prefix: The prefix for filtering the cities array
     */
    func filterCities(withPrefix prefix: String) {
        if prefix.isEmpty {
            searchResults = cities
            return
        }
        searchResults = cities.filter({ (city) -> Bool in
            return city.name.lowercased().hasPrefix(prefix.lowercased())
        })
    }
    
    
    /**
     Get city for a given index. If the index is invalid, then return nil
     
     - Parameters:
        - row: The index of the city in the search results array
     */
    func getCity(forRow row: Int) -> City? {
        if row < 0 || row >= searchResults.count {
            return nil
        }
        return searchResults[row]
    }
    
}
