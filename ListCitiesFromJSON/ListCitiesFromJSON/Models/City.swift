//
//  City.swift
//  ListCitiesFromJSON
//
//  Created by Viktorija Ancevska on 8/11/18.
//  Copyright © 2018 Viktorija Ancevska. All rights reserved.
//

import UIKit
import CoreLocation
import MapKit

///Class that represents the City model
class City: NSObject, MKAnnotation {
    
    static let keyCountryCode = "country"
    static let keyName = "name"
    static let keyID = "_id"
    static let keyCoordinate = "coord"
    static let keyLongitude = "lon"
    static let keyLatitude = "lat"

    var countryCode: String
    var name: String
    var id: Int
    var coordinate: CLLocationCoordinate2D
    
    var printableCoordinate : String {
        return "\(coordinate.longitude), \(coordinate.latitude)"
    }
    
    var title: String? {
        return name
    }
    
    var subtitle: String? {
        return countryCode
    }
    
    /**
     Initializes new city from a dictionary
     
     - Parameters:
        - dictionary: Dictionary that contains all the data needed for creating a City model
     */
    init(withDictionary dictionary: [String: Any]) {
        countryCode = dictionary[City.keyCountryCode] as? String ?? ""
        name = dictionary[City.keyName] as? String ?? ""
        id = dictionary[City.keyID] as? Int ?? -1
        coordinate = CLLocationCoordinate2D(latitude: -1, longitude: -1)
        if let coordinateMap = dictionary[City.keyCoordinate] as? [String: Double] {
            let longitude = coordinateMap[City.keyLongitude] ?? -1
            let latitude = coordinateMap[City.keyLatitude] ?? -1
            coordinate = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
        }
        super.init()
    }
    
}
