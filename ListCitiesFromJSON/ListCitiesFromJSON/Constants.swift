//
//  Constants.swift
//  ListCitiesFromJSON
//
//  Created by Viktorija Ancevska on 8/11/18.
//  Copyright © 2018 Viktorija Ancevska. All rights reserved.
//

import UIKit
import CoreLocation

///Class for storing all the constant properties that will be used in the other classes
class Constants: NSObject {

    static let jsonFilename = "cities"
    static let jsonExtension = ".json"
    
    //cell identifiers
    static let cityCellIdentifier = "CityTableViewCell"
    
    //view controller identifiers
    static let mapControllerIdentifier = "CityMapViewController"
    static let cityListControllerIdentifier = "CitiesViewController"
    
    static let regionRadius: CLLocationDistance = 2000

}
