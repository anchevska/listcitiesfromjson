//
//  CitiesViewController+UITableView.swift
//  ListCitiesFromJSON
//
//  Created by Viktorija Ancevska on 8/11/18.
//  Copyright © 2018 Viktorija Ancevska. All rights reserved.
//

import UIKit

///Class that extends CititesViewController with UITableViewDelegate & UITableViewDataSource methods
extension CitiesViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: Constants.cityCellIdentifier) as? CityTableViewCell,
            let model = CitiesManager.shared.getCity(forRow: indexPath.row) {
            cell.setup(withCityModel: model)
            return cell
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return CitiesManager.shared.searchResults.count
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView()
    }
    
}

extension CitiesViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let mapController = storyboard?.instantiateViewController(withIdentifier: Constants.mapControllerIdentifier) as? CityMapViewController,
            let model = CitiesManager.shared.getCity(forRow: indexPath.row) {
            mapController.city = model
            navigationController?.pushViewController(mapController, animated: true)
        }
    }
    
}


