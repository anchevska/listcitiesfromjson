//
//  CitiesViewController.swift
//  ListCitiesFromJSON
//
//  Created by Viktorija Ancevska on 8/11/18.
//  Copyright © 2018 Viktorija Ancevska. All rights reserved.
//

import UIKit

///Class for showing the list of cities with a search option
class CitiesViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var activityIndicatorView: UIActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initialize()
    }
    
    /// Basic initialization. Load the cities and reload the UI
    func initialize() {
        activityIndicatorView.startAnimating()
        CitiesManager.shared.loadCitiesFromJSON(completionHandler: {(success) in
            self.activityIndicatorView.stopAnimating()
            if success {
                self.tableView.reloadData()
            } else {
                self.showErrorWhileParsing()
            }
        })
    }
    
    ///Show alert informing the user that there was an error reading from the JSON file
    func showErrorWhileParsing() {
        let alertController = UIAlertController(title: "Error", message: "The list of cities is empty since an error occured while parsing the JSON file", preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: .default, handler: {(action) in
            self.dismiss(animated: true, completion: nil)
        })
        alertController.addAction(okAction)
        present(alertController, animated: true, completion: nil)
    }
    
}
