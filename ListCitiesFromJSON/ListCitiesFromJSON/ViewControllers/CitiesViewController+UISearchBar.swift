//
//  CitiesViewController+UISearchBar.swift
//  ListCitiesFromJSON
//
//  Created by Viktorija Ancevska on 8/11/18.
//  Copyright © 2018 Viktorija Ancevska. All rights reserved.
//

import UIKit

///Class that extends CititesViewController with UISearchBarDelegate methods
extension CitiesViewController: UISearchBarDelegate {
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        CitiesManager.shared.filterCities(withPrefix: searchText)
        tableView.reloadData()
        scrollTableToTop()
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.endEditing(true)
    }
    
    ///Scroll the cities list to the top
    fileprivate func scrollTableToTop() {
        if !CitiesManager.shared.searchResults.isEmpty {
            tableView.scrollToRow(at: IndexPath(row: 0, section: 0), at: .top, animated: true)
        }
    }
    
}
