//
//  CityMapViewController.swift
//  ListCitiesFromJSON
//
//  Created by Viktorija Ancevska on 8/11/18.
//  Copyright © 2018 Viktorija Ancevska. All rights reserved.
//

import UIKit
import MapKit

///Class for showing a map centered at a specific previously chosen location
class CityMapViewController: UIViewController {

    @IBOutlet weak var mapView: MKMapView!
    var city: City?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initialize()
    }
    
    ///Basic initialization of the map
    func initialize() {
        if let city = city {
            centerMap(onLocation: city.coordinate)
            mapView.addAnnotation(city)
        }
        title = city?.name
    }
    
    /**
     Center map at a specific location
     
     - Parameters:
        - location: Location coodinate that would indicate the center of the map
     */
    func centerMap(onLocation location: CLLocationCoordinate2D) {
        let coordinateRegion = MKCoordinateRegionMakeWithDistance(location, Constants.regionRadius, Constants.regionRadius)
        mapView.setRegion(coordinateRegion, animated: true)
    }
    
}
