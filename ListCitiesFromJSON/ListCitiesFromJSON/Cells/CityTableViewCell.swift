//
//  CityTableViewCell.swift
//  ListCitiesFromJSON
//
//  Created by Viktorija Ancevska on 8/11/18.
//  Copyright © 2018 Viktorija Ancevska. All rights reserved.
//

import UIKit

///UITableViewCell class representing a City
class CityTableViewCell: UITableViewCell {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var locationLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    /**
     Setup the UI of the cell from a City model
     
     - Parameters:
        - city: The city model that should be shown in that specific cell
     */
    func setup(withCityModel city: City) {
        nameLabel.text = "\(city.name), \(city.countryCode)"
        locationLabel.text = city.printableCoordinate
    }
    
}
